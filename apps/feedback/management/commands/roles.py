from django.core.management.base import BaseCommand, CommandError
from django.db.models.loading import get_model
from django.contrib.auth.models import Group, Permission, User
from annoying.functions import get_object_or_None
from getpass import getpass
import re

SystemUser = get_model('feedback', 'systemuser')

email_regex = re.compile('[^@]+@[^@]+\.[^@]+')

class Command(BaseCommand):
    def handle(self, *args, **options):
        #create admin group
        admin_group, created = Group.objects.get_or_create(name='Admin Group')
        
        #add permissions to admin group
        perms = ('add_employee', 'edit_employee', 'delete_employee', 'add_company', 'change_company', 'delete_company', 'add_systemuser', 'change_systemuser', 'delete_systemuser')
        
        for p in perms:
            perm = Permission.objects.get(codename=p)
            admin_group.permissions.add(perm)
            
        self.stdout.write('Admin group added successfully')
        
        if admin_group.user_set.count():
            self.stdout.write('Found an admin so exiting without creating one')
        else:
            username = None
            while not username:
                username = raw_input('Enter the admin username: ')
                user = get_object_or_None(User, username=username)
                if user:
                    self.stdout.write('Username already taken')
                    username = None
                    
            password = None
            while not password:
                password = getpass('Enter the admin password: ')
                password2 = getpass('Enter the admin password again: ')
                
                if not password or not password2:
                    self.stdout.write('Provide the password..')
                    password = None
                elif password != password2:
                    self.stdout.write('Passwords did not match.')
                    password = None
                    
            email = None
            while not email:
                email = raw_input('Enter the admin email: ')
                if not email_regex.search(email):
                    self.stdout.write('Provide a valid email address')
                    email = None
                    
            user = User.objects.create_user(username, email=email, password=password)
            
            admin_group.user_set.add(user)
            
            #create the admin
            su = SystemUser(role=SystemUser.ADMIN, user=user)
            su.save()
            
        #create employee group
        employee_group, created = Group.objects.get_or_create(name='Employee Group')
        
        #add permissions to employee group
        perms = ('add_feedback',)
        
        for p in perms:
            perm = Permission.objects.get(codename=p)
            employee_group.permissions.add(perm)
            
        self.stdout.write('Employee group added successfully')