from django.conf.urls import patterns, include, url

from feedback.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'customerfeedback.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', LoginPageView.as_view(), name='login-page'),
    url(r'^admin/?$', AdminHomeView.as_view(), name='admin-home'),
    url(r'^logout/?$', LogoutView.as_view(), name='logout'),
    #companies
    url(r'^admin/companies/?$', AdminCompaniesView.as_view(), name='admin-companies'),
    url(r'^admin/companies/create/?$', CreateCompanyView.as_view(), name='create-company'),
    url(r'^admin/companies/(?P<company_id>\d+)/edit/?$', EditCompanyView.as_view(), name='edit-company'),
    url(r'^admin/companies/(?P<company_id>\d+)/delete/?$', DeleteCompanyView.as_view(), name='delete-company'),
    url(r'^admin/companies/(?P<company_id>\d+)/feedback/?$', CompanyFeedbackView.as_view(), name='feedback-company'),
    #admin employees
    url(r'^admin/employees/?$', AdminEmployeesView.as_view(), name='admin-employees'),
    url(r'^admin/employees/create/?$', CreateEmployeeView.as_view(), name='create-employee'),
    url(r'^admin/employees/(?P<employee_id>\d+)/edit/?$', EditEmployeeView.as_view(), name='edit-employee'),
    url(r'^admin/employees/(?P<employee_id>\d+)/delete/?$', DeleteEmployeeView.as_view(), name='delete-employee'),
    url(r'^admin/employees/(?P<employee_id>\d+)/assign-companies/?$', AssignCompanyView.as_view(), name='assign-companies'),
    #employee urls
    url(r'^employee/?$', EmployeeHomeView.as_view(), name='employee-home'),
    url(r'^employee/(?P<company_id>\d+)/feedback/?$', EmployeeFeedbackView.as_view(), name='feedback'),
)

