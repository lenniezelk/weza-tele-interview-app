class MissingCompanyError(Exception):
    pass

class MissingEmployeeGroupError(Exception):
    pass