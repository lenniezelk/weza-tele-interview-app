from django.shortcuts import render, get_object_or_404
from django.views.generic.base import View
from feedback.forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from braces.views import LoginRequiredMixin
from feedback.models import *
from django.core.exceptions import PermissionDenied
from annoying.functions import get_object_or_None
from feedback.exceptions import *
from django.contrib.auth.models import Group

class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('login-page'))

# Create your views here.
class LoginPageView(View):
    def get(self, request):
        if request.user.is_authenticated():
            #is currently  logged in user an admin?
            su = SystemUser.objects.get(user=request.user)
            if su.is_admin:
                return HttpResponseRedirect(reverse('admin-home'))
            elif su.is_employee:
                return HttpResponseRedirect(reverse('employee-home'))
            
        return render(request, 'feedback/login.html',
        {
            'form': LoginForm()
        }
        )
    
    def post(self, request):
        form = LoginForm(request.POST)
        
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            
            user = authenticate(username=username, password=password)
            
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('login-page'))
                else:
                    messages.error(request, 'User is inactive')
                    return HttpResponseRedirect(reverse('login-page'))
            else:
                messages.error(request, 'Invalid username/password')
                return HttpResponseRedirect(reverse('login-page'))
        else:
            messages.error(request, 'Kindly correct the errors in the form')
            return render(request, 'feedback/login.html',
            {
                'form': form
            }
            )
            
class AdminHomeView(LoginRequiredMixin, View):
    def get(self, request):
        su = SystemUser.objects.get(user=request.user)
        if not su.is_admin:
            raise PermissionDenied
            
        return render(request, 'feedback/admin_home.html')
        
#companies       
class AdminCompaniesView(LoginRequiredMixin, View):
    def get(self, request):
        su = SystemUser.objects.get(user=request.user)
        if not su.is_admin:
            raise PermissionDenied
            
        return render(request, 'feedback/admin_companies.html',
        {
            'companies': Company.objects.all()
        }
        )
        
class CreateCompanyView(LoginRequiredMixin, View):
    def get(self, request):
        if not request.user.has_perm('feedback.add_company'):
            raise PermissionDenied
        
        return render(request, 'feedback/create_company.html',
        {
            'form': CompanyForm()
        }
        )
        
    def post(self, request):
        if not request.user.has_perm('feedback.add_company'):
            raise PermissionDenied
        
        form = CompanyForm(request.POST, request.FILES)
        
        if form.is_valid():
            form.save()
            
            messages.success(request, 'Company created successfully.')
            return HttpResponseRedirect(reverse('admin-companies'))
        else:
            messages.error(request, 'Kindly correct the errors in the form')
            return render(request, 'feedback/create_company.html',
            {
                'form': form
            }
            )
            
class EditCompanyView(LoginRequiredMixin, View):
    def get(self, request, company_id):
        if not request.user.has_perm('feedback.change_company'):
            raise PermissionDenied
        
        company = get_object_or_None(Company, pk=company_id)
        
        if not company:
            messages.error(request, 'Company not found.')
            return HttpResponseRedirect(reverse('admin-companies'))
        
        return render(request, 'feedback/edit_company.html',
        {
            'form': CompanyForm(instance=company),
            'company': company
        }
        )
    def post(self, request, company_id):
        if not request.user.has_perm('feedback.change_company'):
            raise PermissionDenied
        
        company = get_object_or_None(Company, pk=company_id)
        
        if not company:
            messages.error(request, 'Company not found.')
            return HttpResponseRedirect(reverse('admin-companies'))
            
        form = CompanyForm(request.POST, request.FILES, instance=company)
        
        if form.is_valid():
            form.save()
            
            messages.success(request, 'Changes saved successfully.')
            return HttpResponseRedirect(reverse('admin-companies'))
        else:
            messages.error(request, 'Kindly correct the errors in the form')
            return render(request, 'feedback/edit_company.html',
            {
                'form': form,
                'company': company
            }
            )
            
class DeleteCompanyView(LoginRequiredMixin, View):
    def get(self, request, company_id):
        if not request.user.has_perm('feedback.delete_company'):
            raise PermissionDenied
        
        company = get_object_or_None(Company, pk=company_id)
        
        if not company:
            messages.error(request, 'Company not found.')
            return HttpResponseRedirect(reverse('admin-companies'))
            
        company.delete()
        
        messages.success(request, 'Company deleted successfully.')
        return HttpResponseRedirect(reverse('admin-companies'))
        
#employees
class AdminEmployeesView(LoginRequiredMixin, View):
    def get(self, request):
        su = SystemUser.objects.get(user=request.user)
        if not su.is_admin:
            raise PermissionDenied
            
        return render(request, 'feedback/admin_employees.html',
        {
            'employees': SystemUser.objects.filter(role=SystemUser.EMPLOYEE)
        }
        )
        
class CreateEmployeeView(LoginRequiredMixin, View):
    def get(self, request):
        if not request.user.has_perm('feedback.add_employee'):
            raise PermissionDenied
        
        return render(request, 'feedback/create_employee.html',
        {
            'form': EmployeeForm()
        }
        )
        
    def post(self, request):
        if not request.user.has_perm('feedback.add_employee'):
            raise PermissionDenied
        
        form = EmployeeForm(request.POST)
        
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            
            employee_group = get_object_or_None(Group, name='Employee Group')
            
            if not employee_group:
                raise MissingEmployeeGroupError('Employee Group not found')
            
            user = User.objects.create_user(username, email=email, password=password)
            
            su = SystemUser()
            su.user = user
            su.user.first_name = first_name
            su.user.last_name = last_name
            su.user.save()
            su.save()
            
            su.user.groups.add(employee_group)
            
            messages.success(request, 'Employee created successfully.')
            return HttpResponseRedirect(reverse('admin-employees'))
        else:
            messages.error(request, 'Kindly correct the errors in the form')
            return render(request, 'feedback/create_employee.html',
            {
                'form': form
            }
            )
            
class EditEmployeeView(LoginRequiredMixin, View):
    def get(self, request, employee_id):
        if not request.user.has_perm('feedback.edit_employee'):
            raise PermissionDenied
        
        su = get_object_or_None(SystemUser, pk=employee_id)
        
        if not su:
            messages.error(request, 'Employee not found.')
            return HttpResponseRedirect(reverse('admin-employees'))
            
        initial = {
            'first_name': su.user.first_name,
            'last_name': su.user.last_name,
            'username': su.user.username,
            'email': su.user.email
            }
        
        return render(request, 'feedback/edit_employee.html',
        {
            'form': EditEmployeeForm(initial=initial),
            'employee': su
        }
        )
    def post(self, request, employee_id):
        if not request.user.has_perm('feedback.edit_employee'):
            raise PermissionDenied
        
        su = get_object_or_None(SystemUser, pk=employee_id)
        
        if not su:
            messages.error(request, 'Employee not found.')
            return HttpResponseRedirect(reverse('admin-employees'))
            
        form = EditEmployeeForm(request.POST)
        
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            
            su.user.username = username
            su.user.email = email
            su.user.first_name = first_name
            su.user.last_name = last_name
            
            su.user.save()
            
            messages.success(request, 'Changes saved successfully.')
            return HttpResponseRedirect(reverse('admin-employees'))
        else:
            messages.error(request, 'Kindly correct the errors in the form')
            return render(request, 'feedback/edit_employee.html',
            {
                'form': form,
                'employee': su
            }
            )
            
class AssignCompanyView(LoginRequiredMixin, View):
    def get(self, request, employee_id):
        if not request.user.has_perm('feedback.change_company'):
            raise PermissionDenied
        
        su = get_object_or_None(SystemUser, pk=employee_id)
        
        if not su:
            messages.error(request, 'Employee not found.')
            return HttpResponseRedirect(reverse('admin-employees'))
            
        assigned_companies = su.assigned_companies.values_list('id', flat=True)
        
        return render(request, 'feedback/assign_companies.html',
        {
            'assigned_companies': assigned_companies,
            'companies': Company.objects.all(),
            'employee': su
        }
        )
    
    def post(self, request, employee_id):
        if not request.user.has_perm('feedback.change_company'):
            raise PermissionDenied
        
        su = get_object_or_None(SystemUser, pk=employee_id)
        
        if not su:
            messages.error(request, 'Employee not found.')
            return HttpResponseRedirect(reverse('admin-employees'))
            
        selected_companies = request.POST.getlist('companies')
        
        current_companies = su.assigned_companies.all()
        
        l = []
        
        for company_id in selected_companies:
            company = get_object_or_None(Company, pk=company_id)
            
            if not company:
                raise MissingCompanyError('Company with id: ' + company_id + ' not found')
            
            su.assigned_companies.add(company)
            
            l.append(company)
            
            
        for company in current_companies:
            if company not in l:
                su.assigned_companies.remove(company)
                
        
        messages.success(request, 'Changes saved successfully.')
        return HttpResponseRedirect(reverse('admin-employees'))   
            
        
class DeleteEmployeeView(LoginRequiredMixin, View):
    def get(self, request, employee_id):
        if not request.user.has_perm('feedback.delete_employee'):
            raise PermissionDenied
        
        su = get_object_or_None(SystemUser, pk=employee_id)
        
        if not su:
            messages.error(request, 'Employee not found.')
            return HttpResponseRedirect(reverse('admin-employees'))
            
        su.delete()
        
        messages.success(request, 'Employee deleted successfully.')
        return HttpResponseRedirect(reverse('admin-employees'))
        
class EmployeeHomeView(LoginRequiredMixin, View):
    def get(self, request):
        if not request.user.has_perm('feedback.add_feedback'):
            raise PermissionDenied
        
        su = get_object_or_404(SystemUser, user=request.user)
        
        return render(request, 'feedback/employee/employee_home.html',
        {
            'companies': su.assigned_companies.all()
        }
        )
            
class EmployeeFeedbackView(LoginRequiredMixin, View):
    def get(self, request, company_id):
        if not request.user.has_perm('feedback.add_feedback'):
            raise PermissionDenied
        
        company = get_object_or_None(Company, pk=company_id)
        
        if not company:
            messages.error(request, 'Company not found.')
            return HttpResponseRedirect(reverse('employee-home'))
            
        return render(request, 'feedback/employee/feedback.html',
        {
            'company': company,
            'form': FeedbackForm()
        }
        )
        
    def post(self, request, company_id):
        if not request.user.has_perm('feedback.add_feedback'):
            raise PermissionDenied
        
        company = get_object_or_None(Company, pk=company_id)
        
        if not company:
            messages.error(request, 'Company not found.')
            return HttpResponseRedirect(reverse('employee-home'))
            
        form = FeedbackForm(request.POST)
        
        if form.is_valid():
            form.save()
            
            messages.success(request, 'Feedback made successfully.')
            return HttpResponseRedirect(reverse('employee-home'))
        else:
            messages.error(request, 'Kindly, correct the errors in the form.')
            return render(request, 'feedback/employee/feedback.html',
            {
                'company': company,
                'form': form
            }
            )
            
class CompanyFeedbackView(LoginRequiredMixin, View):
    def get(self, request, company_id):
        if not request.user.has_perm('feedback.change_company'):
            raise PermissionDenied
        
        company = get_object_or_None(Company, pk=company_id)
        
        if not company:
            messages.error(request, 'Company not found.')
            return HttpResponseRedirect(reverse('admin-home'))
            
        return render(request, 'feedback/company_feedback.html',
        {
            'company': company,
            'feedbacks': company.feedbacks.all()
        }
        )