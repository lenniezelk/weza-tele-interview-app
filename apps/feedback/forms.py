from django import forms
from feedback.models import *

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    
class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ('assigned_employees', )
        
class EmployeeForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    
    
class EditEmployeeForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    username = forms.CharField()
    email = forms.EmailField()
    
class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback