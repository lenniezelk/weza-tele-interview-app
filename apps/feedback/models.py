from django.db import models
from django.contrib.auth.models import User
from annoying.fields import AutoOneToOneField

# Create your models here.

class SystemUser(models.Model):
    ADMIN, EMPLOYEE = range(2)
    ROLE_CODES = (
        (ADMIN, 'Admin'),
        (EMPLOYEE, 'Employee')
    )
    user = AutoOneToOneField(User)
    role = models.IntegerField(choices=ROLE_CODES, default=EMPLOYEE)
    
    class Meta:
        permissions = (
            ('add_employee', 'Add Employee'),
            ('edit_employee', 'Edit Employee'),
            ('delete_employee', 'Delete Employee'),
            ('add_admin', 'Add admin'),
            ('edit_admin', 'Edit admin'),
            ('delete_admin', 'Delete admin'),
        )
    
    def __unicode__(self, ):
        return self.user.first_name
        
    @property
    def is_admin(self, ):
        #check if the user is an admin
        return self.role == SystemUser.ADMIN
        
    @property
    def is_employee(self, ):
        #check if the user is an employeee
        return self.role == SystemUser.EMPLOYEE
        
class Company(models.Model):
    name = models.CharField(max_length=100)
    tagline = models.CharField(max_length=255, null=True, blank=True)
    logo = models.ImageField(upload_to='company_logos', null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    assigned_employees = models.ForeignKey(SystemUser, null=True, blank=True, related_name='assigned_companies')
    
    def __unicode__(self, ):
        return self.name
        
class Feedback(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=20)
    comment = models.TextField()
    company = models.ForeignKey(Company, related_name='feedbacks')
    
    def __unicode__(self, ):
        return 'Feebback from ' + self.first_name + ' for ' + self.company.name